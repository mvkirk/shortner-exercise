from google.appengine.ext import ndb


class Link(ndb.Model):
    url = ndb.StringProperty(required=True)
    shortUrl = ndb.StringProperty()
    date = ndb.DateTimeProperty(auto_now=True)
    views = ndb.IntegerProperty(default=0)
