import logging
import os
import jinja2
import webapp2
import json
from google.appengine.ext.webapp.util import run_wsgi_app
from server import APPLICATION_URL

import manager


jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(os.path.dirname(__file__))),
    variable_start_string='[[',
    variable_end_string=']]'
)


class MainHandler(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template('/client/front/index.html')
        self.response.out.write(template.render({}))

class ShortHandler(webapp2.RequestHandler):
    def get(self):
        shortUrl = self.request.url.split('/')[-1]
        # TODO
        self.redirect(str(link.url))


    def post(self):
        """
        Shorten url
        """
        url = self.request.get('url')
        # TODO

        self.response.write(json.dumps(response))


application = webapp2.WSGIApplication([
    ('/short.*', ShortHandler),
    ('/', MainHandler),
    ('/.*', ShortHandler),
], debug=True)


def main():
    logging.getLogger().setLevel(logging.INFO)
    run_wsgi_app(application)

if __name__ == '__main__':
    main()
