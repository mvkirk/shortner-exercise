angular
.module('shortner', [
    'lumx',
    'Factories',
    'Services',
    'Controllers',
    'Directives'
    ]);


// Modules declaration
angular.module('Factories', ['ngResource']);
angular.module('Services', []);
angular.module('Controllers', []);
angular.module('Directives', []);


// Factories
angular.module('Factories').factory('ShortFactory', function($resource)
{
    return $resource('/short', {},
    {
        shortUrl: { method: 'POST', isArray: false }
        });
    });


// Controllers
angular.module('Controllers').controller('MainController', ['$scope', 'ShortFactory',
function($scope, ShortFactory)
{
    $scope.shortUrl = function(url)
    {
        ShortFactory.shortUrl({url: url}, {}, function(resp)
        {
            $scope.shortUrl = resp.shortUrl;
        }, function(err)
        {

        });
    };
}]);
